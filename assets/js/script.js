function appendNew() {
    const desc   = [
                        "Create initial layout for hompage",
                        "Fixing icons with transparent background",
                        "Discussions regarding workflow improvement",
                        "Create quotation for app redesign project",
                    ]
    const status = ["Approved", "In Progress", "In Review", "Waiting"]

    const random_status = Math.floor(Math.random() * status.length)
    const random_desc = Math.floor(Math.random() * desc.length)
    let random_btn_class = ""

    if (status[random_status] == "Approved")
        random_btn_class = 'approve'
    else if (status[random_status] == "In Progress")
        random_btn_class = 'progress'
    else if (status[random_status] == "In Review")
        random_btn_class = 'review'
    else if (status[random_status] == "Waiting")
        random_btn_class = 'waiting'

    const upcoming_list = document.getElementById('upcomingList')
    upcoming_list.innerHTML += `<div class="d-flex justify-content-between">
                                    <div>
                                        <label class="label">
                                            <input type="radio" class="option-input radio" name="radio5" disabled />
                                            ${desc[random_desc]}
                                        </label>
                                    </div>
                                    <div>
                                        <button class="mt-3 btn-${random_btn_class}">
                                            ${status[random_status]}
                                        </button>
                                    </div>
                                </div>`
}